import { sum } from '../index';

test('basic', () => {
  expect(sum()).toBe(0);
});

test('basic again', () => {
  expect(sum(2, 1)).toBe(3);
});