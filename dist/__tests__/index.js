"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('module-alias/register');
const index_1 = require("../index");
test('basic', () => {
    expect(index_1.sum()).toBe(0);
});
test('basic again', () => {
    expect(index_1.sum(1, 2)).toBe(3);
});
