"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('module-alias/register');
exports.sum = (...a) => a.reduce((acc, val) => acc + val, 0);
